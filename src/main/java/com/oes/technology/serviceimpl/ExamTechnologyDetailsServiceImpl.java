package com.oes.technology.serviceimpl;

import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.oes.exception.OESException;
import com.oes.technology.model.ExamTechnologyDetailsModel;
import com.oes.technology.repository.ExamTechnologyDetailsRepository;
import com.oes.technology.service.ExamTechnologyDetailsService;

@Service
public class ExamTechnologyDetailsServiceImpl implements ExamTechnologyDetailsService{

	@Autowired
	ExamTechnologyDetailsRepository repo;

	@Override
	public ExamTechnologyDetailsModel saveTechnology(ExamTechnologyDetailsModel technologyDetails)
	{
		return repo.save(technologyDetails);
	}
	
	@Override
	public ExamTechnologyDetailsModel updateTechnology(ExamTechnologyDetailsModel technologyDetails)
	{
		if(Objects.isNull(technologyDetails))
		{
			throw new OESException("Invalid Update Request", HttpStatus.BAD_REQUEST);
		}
		return repo.save(technologyDetails);
	}
	
	@Override
	public void deleteTechnology(ExamTechnologyDetailsModel technologyDetails)
	{
		if(Objects.isNull(technologyDetails))
		{
			throw new OESException("Invalid delete Request", HttpStatus.BAD_REQUEST);
		}
		repo.delete(technologyDetails);
	}
	
	@Override
	public ExamTechnologyDetailsModel findByTechnologyId(Integer technologyId)
	{
		return repo.findById(technologyId).get();
	}
	
	@Override
	public ExamTechnologyDetailsModel findByTechnologyName(String technologyName)
	{
		return repo.findBytechnologyName(technologyName);
	}
	
	@Override
	public List<ExamTechnologyDetailsModel> getAllTechnologies()
	{
		return repo.findAll();
	}
}

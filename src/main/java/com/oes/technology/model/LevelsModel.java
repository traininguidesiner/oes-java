package com.oes.technology.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity(name = "levels")
public class LevelsModel {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "level_id")
	private Integer levelId;
	
	@Column(name = "level_name")
	private String levelName;
	
	@Column(name = "level_description")
	private String levelDescription;
	
	
	public LevelsModel() {
		super();
	}
	
	
	public LevelsModel(Integer levelId, String levelName, String levelDescription) {
		super();
		this.levelId = levelId;
		this.levelName = levelName;
		this.levelDescription = levelDescription;
	}


	public Integer getLevelId() {
		return levelId;
	}
	public void setLevelId(Integer levelId) {
		this.levelId = levelId;
	}
	public String getLevelName() {
		return levelName;
	}
	public void setLevelName(String levelName) {
		this.levelName = levelName;
	}
	public String getLevelDescription() {
		return levelDescription;
	}
	public void setLevelDescription(String levelDescription) {
		this.levelDescription = levelDescription;
	}

}

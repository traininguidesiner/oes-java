package com.oes.technology.controller;

import com.oes.commons.BaseDto;
import com.oes.technology.model.UserAttemptedQuestionsModel;
import com.oes.technology.service.UserAttemptedQuestionsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
public class UserAttemptedQuestionsController {

    @Autowired
    UserAttemptedQuestionsService service;

    @PostMapping("/SaveUserAttemptQues")
    public ResponseEntity<BaseDto<UserAttemptedQuestionsModel>> saveUserAttemptQues(@RequestBody UserAttemptedQuestionsModel level) {
        UserAttemptedQuestionsModel response = service.saveUserAttemptedQues(level);
        return new BaseDto(response, "User Attempted Question Saved Successfully", HttpStatus.OK).respond();
    }

    @PutMapping("/UpdateUserAttemptQues")
    public ResponseEntity<BaseDto<UserAttemptedQuestionsModel>> updateUserAttemptQues(@RequestBody UserAttemptedQuestionsModel level) {
        UserAttemptedQuestionsModel response = service.updateUserAttemptedQues(level);
        return new BaseDto(response, "User Attempted Question Info Updated Successfully", HttpStatus.OK).respond();
    }

    @DeleteMapping("/DeleteUserAttemptQues")
    public void deleteUserAttemptQues(@RequestBody UserAttemptedQuestionsModel level) {
        service.deleteUserAttemptedQues(level);
    }

    @GetMapping("/GetByUserAttemptQuesId")
    public ResponseEntity<BaseDto<UserAttemptedQuestionsModel>> getUserAttemptQuesById(@RequestParam Integer levelId) {
        UserAttemptedQuestionsModel response = service.findUserAttemptedQuesById(levelId);
        return new BaseDto(response, "User Attempted Question Retrieved by Id Successfully", HttpStatus.OK).respond();
    }

    @GetMapping("/GetByUserAttemptQuesIdWithoutAnswer")
    public ResponseEntity<BaseDto<UserAttemptedQuestionsModel>> getUserAttemptQuesByIdWithoutAnswer(@RequestParam Integer levelId) {
        UserAttemptedQuestionsModel response = service.findUserAttemptedQuesByIdWithoutAnswer(levelId);
        return new BaseDto(response, "User Attempted Question Without Answer Retrieved by Id Successfully", HttpStatus.OK).respond();
    }

    @GetMapping("/GetAllUserAttemptQues")
    public ResponseEntity<BaseDto<List<UserAttemptedQuestionsModel>>> getAllUserAttemptQues() {
        List<UserAttemptedQuestionsModel> allLevels = service.getAllUserAttemptedQues();
        return new BaseDto(allLevels, "All User Attempted Question Retrived Successfully", HttpStatus.OK).respond();
    }

}

package com.oes.technology.service;


import com.oes.technology.model.UserAttemptedQuestionsModel;

import java.util.List;

public interface UserAttemptedQuestionsService {

    public UserAttemptedQuestionsModel saveUserAttemptedQues (UserAttemptedQuestionsModel level);

    public UserAttemptedQuestionsModel updateUserAttemptedQues(UserAttemptedQuestionsModel level);

    public void deleteUserAttemptedQues(UserAttemptedQuestionsModel level);

    public UserAttemptedQuestionsModel findUserAttemptedQuesById(Integer levelId);


    public UserAttemptedQuestionsModel findUserAttemptedQuesByIdWithoutAnswer(Integer levelId);

    public List<UserAttemptedQuestionsModel> getAllUserAttemptedQues();

}

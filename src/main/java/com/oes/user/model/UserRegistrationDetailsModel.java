package com.oes.user.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity(name="user_registration_details")
public class UserRegistrationDetailsModel {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="user_registration_id")
	private Integer userRegistrationId;
	@Column(name="first_name")
	private String firstName;
	@Column(name="last_name")
	private String lastName;
	@Column(name="email")
	private String email;
	@Column(name="pwd")
	private String password;
	@Column(name="phone_no")
	private String phoneNo;
	
	@Column(name="active_s")
	private Character activeS;
	
	public Integer getUserRegistrationId() {
		return userRegistrationId;
	}
	public void setUserRegistrationId(Integer userRegistrationId) {
		this.userRegistrationId = userRegistrationId;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getPhoneNo() {
		return phoneNo;
	}
	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}
	
	public Character getActiveS() {
		return activeS;
	}
	public void setActiveS(Character activeS) {
		this.activeS = activeS;
	}
	public UserRegistrationDetailsModel(Integer userRegistrationId, String firstName, String lastName, String email,
			String password, String phoneNo, Character activeS) {
		super();
		this.userRegistrationId = userRegistrationId;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.password = password;
		this.phoneNo = phoneNo;
		this.activeS = activeS;
	}
	
	public UserRegistrationDetailsModel() {
		super();
	}
	
	
	
	

}

package com.oes.technology.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.oes.technology.model.ExamCategoryDetailsModel;

public interface ExamCategoryDetailsRepository extends JpaRepository<ExamCategoryDetailsModel, Integer>
{

	@Query("select r from category r where r.categoryName like :categoryName%")
	List<ExamCategoryDetailsModel> findByName(@Param("categoryName") String categoryName);
	
	

	
}

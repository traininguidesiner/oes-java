package com.oes;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication
public class OesJavaApplication {

	public static void main(String[] args) {
		SpringApplication.run(OesJavaApplication.class, args);
	}

}

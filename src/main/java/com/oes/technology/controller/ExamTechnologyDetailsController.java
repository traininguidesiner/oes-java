package com.oes.technology.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.oes.commons.BaseDto;
import com.oes.technology.model.ExamCategoryDetailsModel;
import com.oes.technology.model.ExamTechnologyDetailsModel;
import com.oes.technology.service.ExamTechnologyDetailsService;

import java.util.List;

@RestController
@CrossOrigin
public class ExamTechnologyDetailsController {
	
	@Autowired
	ExamTechnologyDetailsService service;
	
	@PostMapping("/TechnologyEntry")
	public ResponseEntity<BaseDto<ExamCategoryDetailsModel>> saveTechnology(@RequestBody ExamTechnologyDetailsModel technologyDetails)
	{
		ExamTechnologyDetailsModel userDetailsRes = service.saveTechnology(technologyDetails);
		return new BaseDto(userDetailsRes,"Technology Entered Suceesfully",HttpStatus.OK).respond();
	}
	
	@PostMapping("/UpdateTechnology")
	public ResponseEntity<BaseDto<ExamCategoryDetailsModel>> updateTechnology(@RequestBody ExamTechnologyDetailsModel technologyDetails)
	{
		ExamTechnologyDetailsModel userDetailsRes = service.updateTechnology(technologyDetails);
		return new BaseDto(userDetailsRes,"Technology Updated or Null",HttpStatus.OK).respond();
	}
	
	@GetMapping("/GetByTechnologyId")
	public ResponseEntity<BaseDto<ExamCategoryDetailsModel>> getByTechnologyId(@RequestParam Integer technologyId)
	{
		ExamTechnologyDetailsModel userDetailsRes = service.findByTechnologyId(technologyId);
		return new BaseDto(userDetailsRes,"Technology Found Suceesfully",HttpStatus.OK).respond();
	}
	
	@GetMapping("/GetByTechnologyName")
	public ResponseEntity<BaseDto<ExamCategoryDetailsModel>> getByTechnologyName(@RequestParam String technologyName)
	{
		ExamTechnologyDetailsModel userDetailsRes = service.findByTechnologyName(technologyName);
		return new BaseDto(userDetailsRes,"Technology Checked Suceesfully",HttpStatus.OK).respond();
	}
	
	@DeleteMapping("/DeleteTechnology")
	public ResponseEntity<BaseDto<ExamCategoryDetailsModel>> deleteByTechnologyName(@RequestBody ExamTechnologyDetailsModel technologyDetails)
	{
		service.deleteTechnology(technologyDetails);
		return new BaseDto("Technology Deleted Suceesfully",HttpStatus.OK).respond();
	}
	
	@GetMapping("/GetAllTechnologies")
	public ResponseEntity<BaseDto<List<ExamCategoryDetailsModel>>> getAllTechnologies() 
	{
		List<ExamTechnologyDetailsModel> allCategories = service.getAllTechnologies();
		return new BaseDto(allCategories, "All Technologies Retrived Successfully", HttpStatus.OK).respond();
	}

}

package com.oes.technology.serviceimpl;

import com.oes.exception.OESException;
import com.oes.technology.model.TestAttemptsModel;
import com.oes.technology.repository.TestAttemptsRepository;
import com.oes.technology.service.TestAttemptsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

@Service
public class TestAttemptedServiceImpl implements TestAttemptsService {

    @Autowired
    TestAttemptsRepository repo;

    @Override
    public TestAttemptsModel saveTestAttemps(TestAttemptsModel level) {
        return repo.save(level);
    }

    @Override
    public TestAttemptsModel updateTestAttemps(TestAttemptsModel level) {
        if(Objects.isNull(level.getExamId())) {
            throw new OESException("Invalid Update Request", HttpStatus.BAD_REQUEST);
        }
        return repo.save(level);
    }

    @Override
    public void deleteTestAttemps(TestAttemptsModel level) {
        repo.delete(level);
    }

    @Override
    public TestAttemptsModel findTestAttempsById(Integer id) {
        return repo.findById(id).get();
    }

    @Override
    public List<TestAttemptsModel> getAllTestAttemps() {
        return repo.findAll();
    }
}

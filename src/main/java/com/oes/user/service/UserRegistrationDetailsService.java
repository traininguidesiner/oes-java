package com.oes.user.service;

import java.util.List;

import com.oes.user.model.UserRegistrationDetailsModel;

public interface UserRegistrationDetailsService {
	
	public UserRegistrationDetailsModel saveUser(UserRegistrationDetailsModel userRegistrationDetails);
	
	public UserRegistrationDetailsModel updateUser(UserRegistrationDetailsModel userRegistrationDetails);
	
	public void deleteUser(UserRegistrationDetailsModel userRegistrationDetails);
	
	public UserRegistrationDetailsModel findUserByUserId(Integer userId);
	
	public UserRegistrationDetailsModel findUserByUserName(String userName);
	
	public UserRegistrationDetailsModel findUserByUserNameAndPwd(String userName, String password);
	
	public boolean getUserName(String value);
	
	public boolean getPhoneNumber(String phoneNumber);

}

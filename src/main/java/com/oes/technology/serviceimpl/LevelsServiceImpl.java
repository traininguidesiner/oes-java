package com.oes.technology.serviceimpl;

import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.oes.exception.OESException;
import com.oes.technology.model.LevelsModel;
import com.oes.technology.repository.LevelsRepository;
import com.oes.technology.service.LevelsService;

@Service
public class LevelsServiceImpl implements LevelsService {
	
	@Autowired
	LevelsRepository repo;
	
	@Override
	public LevelsModel saveLevel(LevelsModel level) {
		return repo.save(level);
	}

	@Override
	public LevelsModel updateLevel(LevelsModel level) {
		if(Objects.isNull(level.getLevelId())) {
			throw new OESException("Invalid Update Request", HttpStatus.BAD_REQUEST);
		}
		return repo.save(level);
	}

	@Override
	public void deleteLevel(LevelsModel level) {
		repo.delete(level);
	}

	@Override
	public LevelsModel findLevelByLevelId(Integer levelId) {
		return repo.findById(levelId).get();
	}

	@Override
	public LevelsModel findLevelByLevelName(String levelName) {
		return repo.findByLevelName(levelName);
	}

	@Override
	public List<LevelsModel> getAllLevels() {
		return repo.findAll();
	}

}

package com.oes.technology.controller;

import com.oes.commons.BaseDto;
import com.oes.technology.model.TestAttemptsModel;
import com.oes.technology.service.TestAttemptsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@CrossOrigin
@RestController
public class TestAttemptsController {

    @Autowired
    TestAttemptsService service;

    @PostMapping("/SaveTestAttempt")
    public ResponseEntity<BaseDto<TestAttemptsModel>> saveTestAttempt(@RequestBody TestAttemptsModel level) {
        TestAttemptsModel response = service.saveTestAttemps(level);
        return new BaseDto(response, "Test Attempt Saved Successfully", HttpStatus.OK).respond();
    }

    @PutMapping("/UpdateTestAttempt")
    public ResponseEntity<BaseDto<TestAttemptsModel>> updateTestAttempt(@RequestBody TestAttemptsModel level) {
        TestAttemptsModel response = service.updateTestAttemps(level);
        return new BaseDto(response, "Test Attempt Updated Successfully", HttpStatus.OK).respond();
    }

    @DeleteMapping("/DeleteTestAttempt")
    public void deleteTestAttempt(@RequestBody TestAttemptsModel level) {
        service.deleteTestAttemps(level);
    }

    @GetMapping("/GetByTestAttemptId")
    public ResponseEntity<BaseDto<TestAttemptsModel>> getTestAttemptById(@RequestParam Integer levelId) {
        TestAttemptsModel response = service.findTestAttempsById(levelId);
        return new BaseDto(response, "Test Attempt Retrieved by Id Successfully", HttpStatus.OK).respond();
    }

    @GetMapping("/GetAllTestAttempts")
    public ResponseEntity<BaseDto<List<TestAttemptsModel>>> getAllTestAttempt() {
        List<TestAttemptsModel> allLevels = service.getAllTestAttemps();
        return new BaseDto(allLevels, "All Test Attempt Retrived Successfully", HttpStatus.OK).respond();
    }

}

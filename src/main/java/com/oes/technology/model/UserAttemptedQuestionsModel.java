package com.oes.technology.model;

import javax.persistence.*;


@Entity(name = "user_attempted_questions")
public class UserAttemptedQuestionsModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_attempted_questions_id")
    private Integer userAttemptedQuestionsId;

    @JoinColumn(name = "question_id")
    @OneToOne
    private ExamQuestionAndAnswerModel questionId;

    @Column(name = "exam_id")
    private String examId;

    @Column(name = "user_answer")
    private String userAnswer;

    public UserAttemptedQuestionsModel() {
        super();
    }

    public UserAttemptedQuestionsModel(ExamQuestionAndAnswerModel questionId, String examId, String userAnswer) {
        super();
        this.questionId = questionId;
        this.examId = examId;
        this.userAnswer = userAnswer;
    }

    public Integer getUserAttemptedQuestionsId() {
        return userAttemptedQuestionsId;
    }

    public void setUserAttemptedQuestionsId(Integer userAttemptedQuestionsId) {
        this.userAttemptedQuestionsId = userAttemptedQuestionsId;
    }

    public ExamQuestionAndAnswerModel getQuestionId() {
        return questionId;
    }

    public void setQuestionId(ExamQuestionAndAnswerModel questionId) {
        this.questionId = questionId;
    }

    public String getExamId() {
        return examId;
    }

    public void setExamId(String examId) {
        this.examId = examId;
    }

    public String getUserAnswer() {
        return userAnswer;
    }

    public void setUserAnswer(String userAnswer) {
        this.userAnswer = userAnswer;
    }
}

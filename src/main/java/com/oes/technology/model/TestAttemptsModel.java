package com.oes.technology.model;


import com.oes.user.model.UserRegistrationDetailsModel;

import javax.persistence.*;
import java.time.LocalDate;

@Entity(name = "test_attempts")
public class TestAttemptsModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "test_attempts_id")
    private Integer testAttemptsId;

    @Column(name = "exam_id")
    private String examId;

    @Column(name = "no_of_questions_attempted")
    private Integer noOfQuestionsAttempted;

    @Column(name = "no_of_actual_questions")
    private Integer noOfActualQuestions;

    @Column(name = "carried_marks")
    private Integer carriedMarks;

    @Column(name = "scored_marks")
    private Integer scoredMarks;

    @Column(name = "test_status")
    private String testStatus;

    @Column(name = "test_date_and_time")
    private LocalDate testDateAndTime;

    @OneToOne
    @JoinColumn(name = "user_id")
    private UserRegistrationDetailsModel userId;

    @OneToOne
    @JoinColumn(name = "category_Id")
    private ExamCategoryDetailsModel categoryId;

    @OneToOne
    @JoinColumn(name = "technology_id")
    private ExamTechnologyDetailsModel technologyId;

    @OneToOne
    @JoinColumn(name = "level_id")
    private LevelsModel levelId;

    public TestAttemptsModel() { super(); }

    public TestAttemptsModel(String examId, Integer noOfQuestionsAttempted, Integer noOfActualQuestions,
                             Integer carriedMarks, Integer scoredMarks, String testStatus, LocalDate testDateAndTime,
                             UserRegistrationDetailsModel userId, ExamCategoryDetailsModel categoryId,
                             ExamTechnologyDetailsModel technologyId, LevelsModel levelId) {
        super();
        this.examId = examId;
        this.noOfQuestionsAttempted = noOfQuestionsAttempted;
        this.noOfActualQuestions = noOfActualQuestions;
        this.carriedMarks = carriedMarks;
        this.scoredMarks = scoredMarks;
        this.testStatus = testStatus;
        this.testDateAndTime = testDateAndTime;
        this.userId = userId;
        this.categoryId = categoryId;
        this.technologyId = technologyId;
        this.levelId = levelId;
    }

    public Integer getTestAttemptsId() {
        return testAttemptsId;
    }

    public void setTestAttemptsId(Integer testAttemptsId) {
        this.testAttemptsId = testAttemptsId;
    }

    public String getExamId() {
        return examId;
    }

    public void setExamId(String examId) {
        this.examId = examId;
    }

    public Integer getNoOfQuestionsAttempted() {
        return noOfQuestionsAttempted;
    }

    public void setNoOfQuestionsAttempted(Integer noOfQuestionsAttempted) {
        this.noOfQuestionsAttempted = noOfQuestionsAttempted;
    }

    public Integer getNoOfActualQuestions() {
        return noOfActualQuestions;
    }

    public void setNoOfActualQuestions(Integer noOfActualQuestions) {
        this.noOfActualQuestions = noOfActualQuestions;
    }

    public Integer getCarriedMarks() {
        return carriedMarks;
    }

    public void setCarriedMarks(Integer carriedMarks) {
        this.carriedMarks = carriedMarks;
    }

    public Integer getScoredMarks() {
        return scoredMarks;
    }

    public void setScoredMarks(Integer scoredMarks) {
        this.scoredMarks = scoredMarks;
    }

    public String getTestStatus() {
        return testStatus;
    }

    public void setTestStatus(String testStatus) {
        this.testStatus = testStatus;
    }

    public LocalDate getTestDateAndTime() {
        return testDateAndTime;
    }

    public void setTestDateAndTime(LocalDate testDateAndTime) {
        this.testDateAndTime = testDateAndTime;
    }

    public UserRegistrationDetailsModel getUserId() {
        return userId;
    }

    public void setUserId(UserRegistrationDetailsModel userId) {
        this.userId = userId;
    }

    public ExamCategoryDetailsModel getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(ExamCategoryDetailsModel categoryId) {
        this.categoryId = categoryId;
    }

    public ExamTechnologyDetailsModel getTechnologyId() {
        return technologyId;
    }

    public void setTechnologyId(ExamTechnologyDetailsModel technologyId) {
        this.technologyId = technologyId;
    }

    public LevelsModel getLevelId() {
        return levelId;
    }

    public void setLevelId(LevelsModel levelId) {
        this.levelId = levelId;
    }
}

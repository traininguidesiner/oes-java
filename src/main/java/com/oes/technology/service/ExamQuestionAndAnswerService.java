package com.oes.technology.service;

import com.oes.technology.model.ExamQuestionAndAnswerModel;

import java.util.List;

public interface ExamQuestionAndAnswerService {

    public ExamQuestionAndAnswerModel saveQA(ExamQuestionAndAnswerModel qaDetails);

    public ExamQuestionAndAnswerModel updateQA(ExamQuestionAndAnswerModel qaDetails);

    public void deleteQA(ExamQuestionAndAnswerModel qaDetails);

    public ExamQuestionAndAnswerModel findByQAId(Integer qaId);

    public List<ExamQuestionAndAnswerModel> findByQAName(String qaName);

    public List<ExamQuestionAndAnswerModel> findAllQA();


}

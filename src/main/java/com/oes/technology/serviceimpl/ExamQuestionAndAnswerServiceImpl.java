package com.oes.technology.serviceimpl;

import com.oes.exception.OESException;
import com.oes.technology.model.ExamQuestionAndAnswerModel;
import com.oes.technology.repository.ExamQuestionAndAnswerRepository;
import com.oes.technology.service.ExamQuestionAndAnswerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;


@Service
public class ExamQuestionAndAnswerServiceImpl implements ExamQuestionAndAnswerService {

    @Autowired
    ExamQuestionAndAnswerRepository repo;


    @Override
    public ExamQuestionAndAnswerModel saveQA(ExamQuestionAndAnswerModel qaDetails) {
        return repo.save(qaDetails);
    }

    @Override
    public ExamQuestionAndAnswerModel updateQA(ExamQuestionAndAnswerModel qaDetails) {

        if (Objects.isNull(qaDetails.getQuestionId()))
        {
            throw new OESException("invalid update request", HttpStatus.BAD_REQUEST);

        }
        return repo.save(qaDetails);

    }

    @Override
    public void deleteQA(ExamQuestionAndAnswerModel qaDetails) {

        if (Objects.isNull(qaDetails.getQuestionId()))
        {
            throw new OESException("invalid delete request", HttpStatus.BAD_REQUEST);

        }
        repo.delete(qaDetails);
    }

    @Override
    public ExamQuestionAndAnswerModel findByQAId(Integer qaId) {
        return repo.findById(qaId).get();
    }

    @Override
    public List<ExamQuestionAndAnswerModel> findByQAName(String qaName) {
        return repo.findByQuestion(qaName);
    }

    @Override
    public List<ExamQuestionAndAnswerModel> findAllQA() {
        return repo.findAll();
    }


}

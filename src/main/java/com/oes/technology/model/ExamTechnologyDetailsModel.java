package com.oes.technology.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

@Entity(name = "technology")
public class ExamTechnologyDetailsModel {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "technology_id")
	private Integer technologyId;
	@Column(name = "technology_name")
	private String technologyName;
	@Column(name = "technology_description")
	private String technologyDescription;
	@OneToOne
	@JoinColumn(name = "category_Id")
	private ExamCategoryDetailsModel categoryId;

	public Integer getTechnologyId() {
		return technologyId;
	}
	public void setTechnologyId(Integer technologyId) {
		this.technologyId = technologyId;
	}
	public String getTechnologyName() {
		return technologyName;
	}
	public void setTechnologyName(String technologyName) {
		this.technologyName = technologyName;
	}
	public String getTechnologyDescription() {
		return technologyDescription;
	}
	public void setTechnologyDescription(String technologyDescription) {
		this.technologyDescription = technologyDescription;
	}
	public ExamCategoryDetailsModel getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(ExamCategoryDetailsModel categoryId) {
		this.categoryId = categoryId;
	}
	public ExamTechnologyDetailsModel(Integer technologyId, String technologyName, String technologyDescription,
			ExamCategoryDetailsModel categoryId) {
		super();
		this.technologyId = technologyId;
		this.technologyName = technologyName;
		this.technologyDescription = technologyDescription;
		this.categoryId = categoryId;
	}
	public ExamTechnologyDetailsModel() {
		super();
	}
	
	

}

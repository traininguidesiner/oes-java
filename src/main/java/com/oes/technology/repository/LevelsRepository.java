package com.oes.technology.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.oes.technology.model.LevelsModel;

public interface LevelsRepository extends JpaRepository<LevelsModel, Integer> {
	
	LevelsModel findByLevelName(String levelName);

}

package com.oes.technology.service;

import com.oes.technology.DTO.DataTransferModel;

import java.util.ArrayList;
import java.util.Map;


public interface NewExamStartService {


    public Map<String, ArrayList<Integer>> findAllQues(DataTransferModel dto);


}

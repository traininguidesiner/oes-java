package com.oes.technology.controller;


import com.oes.commons.BaseDto;
import com.oes.technology.model.ExamQuestionAndAnswerModel;
import com.oes.technology.repository.ExamQuestionAndAnswerRepository;
import com.oes.technology.service.ExamQuestionAndAnswerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
public class ExamQuestionAndAnswerController {


    @Autowired
    ExamQuestionAndAnswerService service;

    @PostMapping("/QAEntry")
    public ResponseEntity<BaseDto<ExamQuestionAndAnswerModel>> saveQA(@RequestBody ExamQuestionAndAnswerModel
                                                                                               qaDetails)
    {
        ExamQuestionAndAnswerModel userDetailsRes = service.saveQA(qaDetails);
        return new BaseDto(userDetailsRes,"Q & A Entered Suceesfully", HttpStatus.OK).respond();
    }

    @PutMapping("/UpdateQA")
    public ResponseEntity<BaseDto<ExamQuestionAndAnswerModel>> updateQA(@RequestBody ExamQuestionAndAnswerModel qaDetails)
    {
        ExamQuestionAndAnswerModel userDetailsRes = service.updateQA(qaDetails);
        return new BaseDto(userDetailsRes,"Q & A Updated or Null",HttpStatus.OK).respond();
    }

    @GetMapping("/GetByQAId")
    public ResponseEntity<BaseDto<ExamQuestionAndAnswerModel>> getByQAId(@RequestParam Integer qaId)
    {
        ExamQuestionAndAnswerModel userDetailsRes = service.findByQAId(qaId);
        return new BaseDto(userDetailsRes,"Q & A Found Suceesfully",HttpStatus.OK).respond();
    }

    @GetMapping("/GetByQAName")
    public ResponseEntity<BaseDto<List<ExamQuestionAndAnswerModel>>> getByQAName(@RequestParam String qaName)
    {
        List<ExamQuestionAndAnswerModel> userDetailsRes = service.findByQAName(qaName);
        return new BaseDto(userDetailsRes," Q & A Checked Suceesfully",HttpStatus.OK).respond();
    }

    @DeleteMapping("/DeleteQA")
    public ResponseEntity<BaseDto<ExamQuestionAndAnswerModel>> deleteByQAName(@RequestBody ExamQuestionAndAnswerModel qaDetails)
    {
        service.deleteQA(qaDetails);
        return new BaseDto("Q & A Deleted Suceesfully",HttpStatus.OK).respond();
    }

    @GetMapping("/GetAllQA")
    public ResponseEntity<BaseDto<List<ExamQuestionAndAnswerModel>>> getAllTestAttempt() {
        List<ExamQuestionAndAnswerModel> allLevels = service.findAllQA();
        return new BaseDto(allLevels, "All Q&A Retrived Successfully", HttpStatus.OK).respond();
    }





}

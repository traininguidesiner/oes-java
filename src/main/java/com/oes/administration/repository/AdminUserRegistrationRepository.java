package com.oes.administration.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.oes.administration.model.AdminUserRegistrationModel;

public interface AdminUserRegistrationRepository extends JpaRepository<AdminUserRegistrationModel, Integer>
{

	AdminUserRegistrationModel findByEmail(String adminEmail);

	@Query("select r from admin_user_details r where r.email=:adminUname and r.password=:password")
	AdminUserRegistrationModel findByEmailAndPwd(@Param("adminUname") String adminUname, @Param("password") String password);

	@Query("select r from admin_user_details r where r.email=:userName")
	List<AdminUserRegistrationModel> findByUserName(@Param("userName") String userName);

	
	
	
}

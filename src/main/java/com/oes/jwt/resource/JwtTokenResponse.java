package com.oes.jwt.resource;

import java.io.Serializable;

public class JwtTokenResponse implements Serializable {

	private static final long serialVersionUID = 8317676219297719109L;

	private final String token;

	private  Integer id;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getToken() {
		return token;
	}

	public JwtTokenResponse(String token) {
		this.token = token;
	}

	public JwtTokenResponse(String token, Integer id) {
		super();
		this.token = token;
		this.id = id;
	}
}
package com.oes.administration.service;

import com.oes.administration.model.AdminRolesModel;

public interface AdminRolesService {
	
	public AdminRolesModel saveAdminRole(AdminRolesModel adminDetails);
	
	public AdminRolesModel updateAdminRole(AdminRolesModel adminDetails);
	
	public void deleteAdminRole(AdminRolesModel adminDetails);
	
	public AdminRolesModel findAdminRoleById(Integer adminId);
	
	public AdminRolesModel findAdminRoleByName(String adminUname);
	
	//public boolean getAll(String value);

}

package com.oes.technology.repository;

import com.oes.technology.model.ExamCategoryDetailsModel;
import com.oes.technology.model.ExamQuestionAndAnswerModel;
import com.oes.technology.model.ExamTechnologyDetailsModel;
import com.oes.technology.model.LevelsModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ExamQuestionAndAnswerRepository extends JpaRepository<ExamQuestionAndAnswerModel, Integer> {


    @Query("select r from question_and_answers r where r.question like :qaName%")
    List<ExamQuestionAndAnswerModel> findByQuestion(@Param("qaName") String qaName);


    /*
    userId;
    private LevelsModel levelsModel;
    private ExamCategoryDetailsModel examCategoryDetailsModel;
    private ExamTechnologyDetailsModel examTechnologyDetailsModel;
    private Integer numOfQuesions;
     */
    @Query("select r from question_and_answers r where r.levelId = :levelModel " +
                                                "and r.categoryId = :categoryModel " +
                                                "and r.technologyId = :technologyModel")
    List<ExamQuestionAndAnswerModel> findAllQuestions(@Param("levelModel") LevelsModel levelsModel,
                                                @Param("categoryModel") ExamCategoryDetailsModel categoryModel,
                                                @Param("technologyModel") ExamTechnologyDetailsModel technologyModel);
}

package com.oes.technology.DTO;

import com.oes.technology.model.ExamCategoryDetailsModel;
import com.oes.technology.model.ExamTechnologyDetailsModel;
import com.oes.technology.model.LevelsModel;
import com.oes.user.model.UserRegistrationDetailsModel;

public class DataTransferModel {


    private UserRegistrationDetailsModel userId;
    private LevelsModel levelsModel;
    private ExamCategoryDetailsModel examCategoryDetailsModel;
    private ExamTechnologyDetailsModel examTechnologyDetailsModel;
    private Integer numOfQuestions;

    public DataTransferModel() {
        super();
    }

    public DataTransferModel(LevelsModel levelsModel, ExamCategoryDetailsModel examCategoryDetailsModel, ExamTechnologyDetailsModel examTechnologyDetailsModel, Integer numOfQuestions) {
        super();
        this.levelsModel = levelsModel;
        this.examCategoryDetailsModel = examCategoryDetailsModel;
        this.examTechnologyDetailsModel = examTechnologyDetailsModel;
        this.numOfQuestions = numOfQuestions;
    }

    public UserRegistrationDetailsModel getUserId() {
        return userId;
    }

    public void setUserId(UserRegistrationDetailsModel userId) {
        this.userId = userId;
    }

    public LevelsModel getLevelsModel() {
        return levelsModel;
    }

    public void setLevelsModel(LevelsModel levelsModel) {
        this.levelsModel = levelsModel;
    }

    public ExamCategoryDetailsModel getExamCategoryDetailsModel() {
        return examCategoryDetailsModel;
    }

    public void setExamCategoryDetailsModel(ExamCategoryDetailsModel examCategoryDetailsModel) {
        this.examCategoryDetailsModel = examCategoryDetailsModel;
    }

    public ExamTechnologyDetailsModel getExamTechnologyDetailsModel() {
        return examTechnologyDetailsModel;
    }

    public void setExamTechnologyDetailsModel(ExamTechnologyDetailsModel examTechnologyDetailsModel) {
        this.examTechnologyDetailsModel = examTechnologyDetailsModel;
    }

    public Integer getNumOfQuestions() {
        return numOfQuestions;
    }

    public void setNumOfQuestions(Integer numOfQuestions) {
        this.numOfQuestions = numOfQuestions;
    }
}

package com.oes.technology.service;

import java.util.List;

import com.oes.technology.model.ExamTechnologyDetailsModel;

public interface ExamTechnologyDetailsService {
	
	public ExamTechnologyDetailsModel saveTechnology(ExamTechnologyDetailsModel technologyDetails);
	
	public ExamTechnologyDetailsModel updateTechnology(ExamTechnologyDetailsModel technologyDetails);
	
	public void deleteTechnology(ExamTechnologyDetailsModel technologyDetails);
	
	public ExamTechnologyDetailsModel findByTechnologyId(Integer technologyId);
	
	public ExamTechnologyDetailsModel findByTechnologyName(String technologyName);
	
	public List<ExamTechnologyDetailsModel> getAllTechnologies();
	
}

package com.oes.technology.controller;


import com.oes.commons.BaseDto;
import com.oes.technology.DTO.DataTransferModel;
import com.oes.technology.model.UserAttemptedQuestionsModel;
import com.oes.technology.service.NewExamStartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@RestController
@CrossOrigin
public class StartExamDetailsController {

    @Autowired
    NewExamStartService service;

    @PostMapping(name = "/StartNewTest")
    public ResponseEntity startTest(@RequestBody DataTransferModel dataTM){

        Map<String, ArrayList<Integer>> response = service.findAllQues(dataTM);

        return new BaseDto(response, "Test Attempt Saved Successfully", HttpStatus.OK).respond();

    }



}

package com.oes.technology.service;

import com.oes.technology.model.TestAttemptsModel;

import java.util.List;

public interface TestAttemptsService {

    public TestAttemptsModel saveTestAttemps(TestAttemptsModel level);

    public TestAttemptsModel updateTestAttemps(TestAttemptsModel level);

    public void deleteTestAttemps(TestAttemptsModel level);

    public TestAttemptsModel findTestAttempsById(Integer levelId);

    public List<TestAttemptsModel> getAllTestAttemps();

}

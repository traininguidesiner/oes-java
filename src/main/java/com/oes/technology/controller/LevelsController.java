package com.oes.technology.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.oes.commons.BaseDto;
import com.oes.technology.model.LevelsModel;
import com.oes.technology.service.LevelsService;

@RestController
@CrossOrigin
public class LevelsController {
	
	@Autowired
	LevelsService service;
	
	@PostMapping("/RegisterLevel")
	public ResponseEntity<BaseDto<LevelsModel>> saveLevel(@RequestBody LevelsModel level) {
		LevelsModel response = service.saveLevel(level);
		return new BaseDto(response, "Level Registered Successfully", HttpStatus.OK).respond();
	}
	
	@PutMapping("/UpdateLevel")
	public ResponseEntity<BaseDto<LevelsModel>> updateLevel(@RequestBody LevelsModel level) {
		LevelsModel response = service.updateLevel(level);
		return new BaseDto(response, "Level Info Updated Successfully", HttpStatus.OK).respond();
	}
	
	@DeleteMapping("/DeleteLevel")
	public void deleteLevel(@RequestBody LevelsModel level) {
		service.deleteLevel(level);
	}
	
	@GetMapping("/GetByLevelId")
	public ResponseEntity<BaseDto<LevelsModel>> getLevelById(@RequestParam Integer levelId) {
		LevelsModel response = service.findLevelByLevelId(levelId);
		return new BaseDto(response, "Level Retrieved by Id Successfully", HttpStatus.OK).respond();
	}
	
	@GetMapping("/GetByLevelName")
	public ResponseEntity<BaseDto<LevelsModel>> getLevelByName(@RequestParam String levelName) {
		LevelsModel response = service.findLevelByLevelName(levelName);
		return new BaseDto(response, "Level Retrieved by Name Successfully", HttpStatus.OK).respond();
	}
	
	@GetMapping("/GetAllLevels")
	public ResponseEntity<BaseDto<List<LevelsModel>>> getAllLevels() {
		List<LevelsModel> allLevels = service.getAllLevels();
		return new BaseDto(allLevels, "All Levels Retrived Successfully", HttpStatus.OK).respond();
	}

}

package com.oes.technology.repository;

import com.oes.technology.model.TestAttemptsModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface TestAttemptsRepository extends JpaRepository<TestAttemptsModel, Integer> {

    @Query("select r from test_attempts r where r.examId = :examId")
    List<TestAttemptsModel> findUniqExamId(@Param("examId") String examId);


}

package com.oes.technology.service;

import java.util.List;

import com.oes.technology.model.LevelsModel;

public interface LevelsService {
	
	public LevelsModel saveLevel(LevelsModel level);
	
	public LevelsModel updateLevel(LevelsModel level);
	
	public void deleteLevel(LevelsModel level);
	
	public LevelsModel findLevelByLevelId(Integer levelId);
	
	public LevelsModel findLevelByLevelName(String levelName);
	
	public List<LevelsModel> getAllLevels();

}

package com.oes.technology.service;

import java.util.List;

import com.oes.technology.model.ExamCategoryDetailsModel;
import com.oes.technology.model.LevelsModel;

public interface ExamCategoryDetailsService {

	public ExamCategoryDetailsModel saveCategory(ExamCategoryDetailsModel examCategoryDetailsModel); 
	
	public ExamCategoryDetailsModel updateCategory(ExamCategoryDetailsModel examCategoryDetailsModel); 
	
	public void deleteCategory(ExamCategoryDetailsModel examCategoryDetailsModel); 
	
	public ExamCategoryDetailsModel findCategoryByCategoryId(Integer categoryId); 
	
	public List<ExamCategoryDetailsModel> findCategoryByCategoryName(String categoryName); 
	
	public List<ExamCategoryDetailsModel> getAllCategories();
	 
}

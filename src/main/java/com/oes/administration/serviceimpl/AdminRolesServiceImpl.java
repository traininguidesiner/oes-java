package com.oes.administration.serviceimpl;

import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.oes.administration.model.AdminRolesModel;
import com.oes.administration.repository.AdminRolesRepository;
import com.oes.administration.service.AdminRolesService;
import com.oes.exception.OESException;

@Service
public class AdminRolesServiceImpl implements AdminRolesService
{

	@Autowired
	AdminRolesRepository repo;
	
	@Override
	public AdminRolesModel saveAdminRole(AdminRolesModel adminDetails) 
	{
		return repo.save(adminDetails);
	}

	@Override
	public AdminRolesModel updateAdminRole(AdminRolesModel adminDetails)
	{
		if(Objects.isNull(adminDetails.getAdminRoleId()))
			{
				throw new OESException("invalid update request", HttpStatus.BAD_REQUEST);
			}
		return repo.save(adminDetails);
	}

	@Override
	public void deleteAdminRole(AdminRolesModel adminDetails)
	{
		repo.delete(adminDetails);
	}

	@Override
	public AdminRolesModel findAdminRoleById(Integer adminId)
	{
		return repo.findById(adminId).get();
	}
	
	@Override
	public AdminRolesModel findAdminRoleByName(String adminRoleName)
	{
		return repo.findByAdminRoleName(adminRoleName);
	}
	

	//@Override
	//public boolean getAll(String value)
	//{
	//	List<AdminRolesModel> list1 = repo.findByRoleName(value);
	//	boolean flag = false;
	//	if(list1.size()>0) {
	//		flag = true;
	//	}
	//	return flag;
	//}
}

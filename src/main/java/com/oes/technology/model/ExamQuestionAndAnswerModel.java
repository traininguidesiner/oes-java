package com.oes.technology.model;

import javax.persistence.*;

@Entity(name = "question_and_answers")
public class ExamQuestionAndAnswerModel {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "question_id")
    private Integer questionId;

    @Column(name = "question")
    private String question;

    @Column(name = "option_a")
    private String optionA;

    @Column(name = "option_b")
    private String optionB;

    @Column(name = "option_c")
    private String optionC;

    @Column(name = "option_d")
    private String optionD;

    @Column(name = "answer")
    private String answer;

    @OneToOne
    @JoinColumn(name = "category_Id")
    private ExamCategoryDetailsModel categoryId;

    @OneToOne
    @JoinColumn(name = "technology_id")
    private ExamTechnologyDetailsModel technologyId;

    @OneToOne
    @JoinColumn(name = "level_id")
    private LevelsModel levelId;

    public ExamQuestionAndAnswerModel() {
        super();
    }

    public ExamQuestionAndAnswerModel(Integer questionId, String question,
                                      String optionA, String optionB,
                                      String optionC, String optionD, String answer,
                                      ExamCategoryDetailsModel categoryId,
                                      ExamTechnologyDetailsModel technologyId, LevelsModel levelId) {
        super();
        this.questionId = questionId;
        this.question = question;
        this.optionA = optionA;
        this.optionB = optionB;
        this.optionC = optionC;
        this.optionD = optionD;
        this.answer = answer;
        this.categoryId = categoryId;
        this.technologyId = technologyId;
        this.levelId = levelId;
    }

    public Integer getQuestionId() {
        return questionId;
    }

    public void setQuestionId(Integer questionId) {
        this.questionId = questionId;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getOptionA() {
        return optionA;
    }

    public void setOptionA(String optionA) {
        this.optionA = optionA;
    }

    public String getOptionB() {
        return optionB;
    }

    public void setOptionB(String optionB) {
        this.optionB = optionB;
    }

    public String getOptionC() {
        return optionC;
    }

    public void setOptionC(String optionC) {
        this.optionC = optionC;
    }

    public String getOptionD() {
        return optionD;
    }

    public void setOptionD(String optionD) {
        this.optionD = optionD;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public ExamCategoryDetailsModel getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(ExamCategoryDetailsModel categoryId) {
        this.categoryId = categoryId;
    }

    public ExamTechnologyDetailsModel getTechnologyId() {
        return technologyId;
    }

    public void setTechnologyId(ExamTechnologyDetailsModel technologyId) {
        this.technologyId = technologyId;
    }

    public LevelsModel getLevelId() {
        return levelId;
    }

    public void setLevelId(LevelsModel levelId) {
        this.levelId = levelId;
    }






}

package com.oes.technology.serviceimpl;

import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.oes.exception.OESException;
import com.oes.technology.model.ExamCategoryDetailsModel;
import com.oes.technology.repository.ExamCategoryDetailsRepository;
import com.oes.technology.service.ExamCategoryDetailsService;

@Service
public class ExamCategoryDetailsServiceImpl implements ExamCategoryDetailsService
{
	@Autowired
	ExamCategoryDetailsRepository repo;
	
	@Override
	public ExamCategoryDetailsModel saveCategory(ExamCategoryDetailsModel examCategoryDetailsModel)
	{
		return repo.save(examCategoryDetailsModel);
	}
	
	@Override
	public ExamCategoryDetailsModel updateCategory(ExamCategoryDetailsModel examCategoryDetailsModel)
	{
		if (Objects.isNull(examCategoryDetailsModel.getCategoryId()))
		{
			 throw new OESException("invalid update request", HttpStatus.BAD_REQUEST);
			
		}
		return repo.save(examCategoryDetailsModel);
	}
	
	@Override
	public void deleteCategory(ExamCategoryDetailsModel examCategoryDetailsModel)
	{
		if (Objects.isNull(examCategoryDetailsModel.getCategoryId()))
		{
			 throw new OESException("invalid delete request", HttpStatus.BAD_REQUEST);
			
		}
		repo.delete(examCategoryDetailsModel);
	}

	@Override
	public ExamCategoryDetailsModel findCategoryByCategoryId(Integer categoryId)
	{
		return repo.findById(categoryId).get();
	}
	
	@Override
	public List<ExamCategoryDetailsModel> findCategoryByCategoryName(String categoryName)
	{
		return repo.findByName(categoryName);
	}
	
	@Override
	public List<ExamCategoryDetailsModel> getAllCategories()
	{
		return repo.findAll();
	}
}

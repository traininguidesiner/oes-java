package com.oes.technology.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.oes.technology.model.ExamTechnologyDetailsModel;

public interface ExamTechnologyDetailsRepository extends JpaRepository<ExamTechnologyDetailsModel,Integer>
{

	ExamTechnologyDetailsModel findBytechnologyName(String technologyName);
	
}

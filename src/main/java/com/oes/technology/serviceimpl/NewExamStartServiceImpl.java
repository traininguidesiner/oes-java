package com.oes.technology.serviceimpl;

import com.oes.exception.OESException;
import com.oes.technology.DTO.DataTransferModel;
import com.oes.technology.model.*;
import com.oes.technology.repository.ExamQuestionAndAnswerRepository;
import com.oes.technology.repository.TestAttemptsRepository;
import com.oes.technology.repository.UserAttemptedQuestionsRepository;
import com.oes.technology.service.NewExamStartService;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;


import java.lang.reflect.Array;
import java.time.LocalDate;
import java.util.*;

@Service
public class NewExamStartServiceImpl implements NewExamStartService {

    @Autowired
    ExamQuestionAndAnswerRepository examQuestionAndAnswerRepository;

    @Autowired
    TestAttemptsRepository testAttemptsRepository;

    @Autowired
    UserAttemptedQuestionsRepository userAttemptedQuestionsRepository;
    
    // generates random & unique QAkeys with given range.
    public static ArrayList<Integer> randomIntGenerator(int noOfQues, ArrayList<Integer> qaKeys) {

        HashSet<Integer> hs = new HashSet<>();
        ArrayList<Integer> uniqueKeys = new ArrayList<>();
        Random rand = new Random(); //generates random value
        Integer int_random;

        while (hs.size() != noOfQues && qaKeys.size() >= noOfQues) {
            int_random = qaKeys.get(rand.nextInt(qaKeys.size()));
            if(!hs.contains(int_random))
            { hs.add(int_random); }
        }

        for (int i : hs) { uniqueKeys.add(i); }

        return uniqueKeys;
    }

    public boolean uniqueExamId(String examId) {
        List<TestAttemptsModel> allTestAttempted = testAttemptsRepository.findUniqExamId(examId); // getting all exam_id from test_attempts
        if(allTestAttempted.isEmpty())
        { return false; }
        return true;
    }


    @Override
    public Map<String, ArrayList<Integer>> findAllQues(DataTransferModel dto) {

        List<ExamQuestionAndAnswerModel> questionsList = examQuestionAndAnswerRepository.findAllQuestions(dto.getLevelsModel(),
                dto.getExamCategoryDetailsModel(), dto.getExamTechnologyDetailsModel());

        ArrayList<Integer> qaKeys = new ArrayList<>();

        for (ExamQuestionAndAnswerModel qa : questionsList) { // get all pk from questionsList to arraylist
            qaKeys.add(qa.getQuestionId());
        }

        int numOfQues = dto.getNumOfQuestions();

        ArrayList<Integer> newUniqueKeys;

        if(numOfQues <= qaKeys.size()) {
            newUniqueKeys = randomIntGenerator(numOfQues, qaKeys); // func call Getting random keys for test
        } else {
            throw new OESException("Number od questions requested exceeds the limit\"", HttpStatus.BAD_REQUEST);
        }


        String examId = RandomStringUtils.random(16, true, true); // generating unique exam_id string, with char and nums of 16 digit.

        while (uniqueExamId(examId)){ // checking in db for uniqueness
            examId = RandomStringUtils.random(16, true, true);
        }

        TestAttemptsModel testAttempts = new TestAttemptsModel();   // adding record in test_attempts db, with exam_id
        testAttempts.setExamId(examId);                             // setting values and saving in db
        testAttempts.setNoOfActualQuestions(numOfQues);
        testAttempts.setUserId(dto.getUserId());
        testAttempts.setCategoryId(dto.getExamCategoryDetailsModel());
        // testAttempts.setTestDateAndTime(null);
        testAttempts.setTechnologyId(dto.getExamTechnologyDetailsModel());
        testAttempts.setLevelId(dto.getLevelsModel());
        testAttemptsRepository.save(testAttempts);

        for(int i = 0; i < newUniqueKeys.size(); i++) {                                                   // using exam_id and each QA_id adding record in user_attempted_questions
            UserAttemptedQuestionsModel userAttemptedQuestions = new UserAttemptedQuestionsModel();       // set and save
            userAttemptedQuestions.setExamId(examId);
            ExamQuestionAndAnswerModel exm = new ExamQuestionAndAnswerModel();
            exm.setQuestionId(newUniqueKeys.get(i));
            userAttemptedQuestions.setQuestionId(exm);
            userAttemptedQuestionsRepository.save(userAttemptedQuestions);
        }

        // list of user attempted questions based on exam id
        List<UserAttemptedQuestionsModel> uaqm = userAttemptedQuestionsRepository.findAllUserAttemptedQuesId(examId);

        ArrayList<Integer> userAttemptedQuesId = new ArrayList<>();

        for (UserAttemptedQuestionsModel user: uaqm) {
            userAttemptedQuesId.add(user.getUserAttemptedQuestionsId());
        }

        Map<String, ArrayList<Integer>> userAttemptQues = new HashMap<>();

        userAttemptQues.put(examId, userAttemptedQuesId);

        return userAttemptQues;
    }


//        public static void main(String[] args) {
//
//        ArrayList<Integer> list = new ArrayList<>();
//        list.add(2);list.add(5);
//        list.add(7);list.add(22);
//
//        ArrayList<Integer> ins = randomIntGenerator(5, list);
//
//        for (Integer i : ins) {
//            System.out.print(i + " > ");
//        }
//
//        String s = RandomStringUtils.random(16, true, true);
//        System.out.println();
//        System.out.println(s);
//    }


}

package com.oes.technology.serviceimpl;

import com.oes.exception.OESException;
import com.oes.technology.model.ExamQuestionAndAnswerModel;
import com.oes.technology.model.UserAttemptedQuestionsModel;
import com.oes.technology.repository.UserAttemptedQuestionsRepository;
import com.oes.technology.service.UserAttemptedQuestionsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

@Service
public class UserAttemptedQuestionsServiceImpl implements UserAttemptedQuestionsService {


    @Autowired
    UserAttemptedQuestionsRepository repo;

    @Override
    public UserAttemptedQuestionsModel saveUserAttemptedQues(UserAttemptedQuestionsModel level) {
        return repo.save(level);
    }

    @Override
    public UserAttemptedQuestionsModel updateUserAttemptedQues(UserAttemptedQuestionsModel level) {
        if(Objects.isNull(level.getExamId())) {
            throw new OESException("Invalid Update Request", HttpStatus.BAD_REQUEST);
        }
        return repo.save(level);
    }

    @Override
    public void deleteUserAttemptedQues(UserAttemptedQuestionsModel level) {
        repo.delete(level);
    }

    @Override
    public UserAttemptedQuestionsModel findUserAttemptedQuesById(Integer levelId) {
        return repo.findById(levelId).get();
    }

    @Override
    public UserAttemptedQuestionsModel findUserAttemptedQuesByIdWithoutAnswer(Integer levelId) {
        UserAttemptedQuestionsModel usr = repo.findById(levelId).get();
        ExamQuestionAndAnswerModel exm = usr.getQuestionId();
        exm.setAnswer(null);
        usr.setQuestionId(exm);
        return usr;
    }

    @Override
    public List<UserAttemptedQuestionsModel> getAllUserAttemptedQues() {
        return repo.findAll();
    }




}

package com.oes.technology.repository;

import com.oes.technology.model.UserAttemptedQuestionsModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface UserAttemptedQuestionsRepository extends JpaRepository<UserAttemptedQuestionsModel, Integer> {


    @Query("select r from user_attempted_questions r where r.examId = :examId")
    List<UserAttemptedQuestionsModel> findAllUserAttemptedQuesId(@Param("examId") String examId);

}

package com.oes.technology.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.oes.commons.BaseDto;
import com.oes.technology.model.ExamCategoryDetailsModel;
import com.oes.technology.model.LevelsModel;
import com.oes.technology.service.ExamCategoryDetailsService;

@RestController
@CrossOrigin
public class ExamCategoryDetailsController {

@Autowired	
ExamCategoryDetailsService service;

@PostMapping("/CategoryEntry")
public ResponseEntity<BaseDto<ExamCategoryDetailsModel>> saveCategory(@RequestBody ExamCategoryDetailsModel categoryDetails)
{
	ExamCategoryDetailsModel userDetailsRes = service.saveCategory(categoryDetails);
	return new BaseDto(userDetailsRes,"Category Entered Suceesfully",HttpStatus.OK).respond();
}

@PostMapping("/UpdateCategory")
public ResponseEntity<BaseDto<ExamCategoryDetailsModel>> updateCategory(@RequestBody ExamCategoryDetailsModel categoryDetails)
{
	ExamCategoryDetailsModel userDetailsRes = service.updateCategory(categoryDetails);
	return new BaseDto(userDetailsRes,"Updated or Null",HttpStatus.OK).respond();
}

@GetMapping("/GetByCategoryId")
public ResponseEntity<BaseDto<ExamCategoryDetailsModel>> getCategoryByCategoryId(@RequestParam Integer categoryId)
{
	ExamCategoryDetailsModel userDetailsRes = service.findCategoryByCategoryId(categoryId);	
	return new BaseDto(userDetailsRes,"Category Found Suceesfully",HttpStatus.OK).respond();
}

@DeleteMapping("/DeleteCategory")
public ResponseEntity<BaseDto<ExamCategoryDetailsModel>> deleteCategory(@RequestBody ExamCategoryDetailsModel categoryDetails)
{
	service.deleteCategory(categoryDetails);
	return new BaseDto("Deleted Successfully", HttpStatus.OK).respond(); 
}

@GetMapping("/GetByCategoryName")
public ResponseEntity<BaseDto<List<ExamCategoryDetailsModel>>> getByCategoryName(@RequestParam String categoryName)
{
	List<ExamCategoryDetailsModel> userDetailsRes = service.findCategoryByCategoryName(categoryName);
	return new BaseDto(userDetailsRes,"Category Checked Suceesfully",HttpStatus.OK).respond();
}

@GetMapping("/GetAllCategories")
public ResponseEntity<BaseDto<List<ExamCategoryDetailsModel>>> getAllCategories() 
{
	List<ExamCategoryDetailsModel> allCategories = service.getAllCategories();
	return new BaseDto(allCategories, "All Categories Retrived Successfully", HttpStatus.OK).respond();
}




}
